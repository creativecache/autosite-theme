<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$sidebar = get_theme_mod( 'autosite_sidebar_position' );

?>

<aside class="sidebar<?php if( $sidebar == 'left' ) { echo ' left'; } else if ( $sidebar == 'right' ) { echo ' right'; } ?>">

	<?php if ( is_active_sidebar( 'main-sidebar' ) ) : ?>
		<ul id="main-sidebar" class="widget-area">
			<?php dynamic_sidebar( 'main-sidebar' ); ?>
		</ul>
	<?php endif; ?>

</aside>