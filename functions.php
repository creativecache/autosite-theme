<?php

if ( ! function_exists( 'ascm_fs' ) ) {
	// Create a helper function for easy SDK access.
	function ascm_fs() {
		global $ascm_fs;

		if ( ! isset( $ascm_fs ) ) {
			// Include Freemius SDK.
			require_once dirname(__FILE__) . '/freemius/start.php';

			$ascm_fs = fs_dynamic_init( array(
				'id'                  => '4749',
				'slug'                => 'autosite',
				'type'                => 'theme',
				'public_key'          => 'pk_084e2fbe81cd9f4990d9394c679cf',
				'is_premium'          => true,
				'premium_suffix'      => 'Plus',
				// If your theme is a serviceware, set this option to false.
				'has_premium_version' => true,
				'has_addons'          => true,
				'has_paid_plans'      => true,
				'trial'               => array(
					'days'               => 30,
					'is_require_payment' => false,
				),
				'menu'                => array(
					'support'        => false,
				),
				// Set the SDK to work in a sandbox mode (for development & testing).
				// IMPORTANT: MAKE SURE TO REMOVE SECRET KEY BEFORE DEPLOYMENT.
				'secret_key'          => 'sk_>FB_&nPS;KCHzFN>pB)Q@9$w+>4.0',
			) );
		}

		return $ascm_fs;
	}

	// Init Freemius.
	ascm_fs();
	// Signal that SDK was initiated.
	do_action( 'ascm_fs_loaded' );
}

$autosite_includes = array(
	'/default-settings.php',
	'/theme_support.php',
	'/register.php',
	'/sanitizations.php',
	'/customizer.php',
	'/enqueue.php',
	'/widgets.php',
	'/menus.php',
	'/breadcrumbs.php',
	'/autosite-functions.php',
);

foreach ( $autosite_includes as $file ) {
	$filepath = locate_template( 'inc' . $file );
	if ( ! $filepath ) {
		trigger_error( sprintf( 'Error locating /inc%s for inclusion', $file ), E_USER_ERROR );
	}
	require_once $filepath;
}