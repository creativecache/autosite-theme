<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$sidebar = get_theme_mod( 'autosite_sidebar_position' );
$post_sidebar = get_theme_mod( 'autosite_post_sidebar' );

get_header(); 

if ( ($sidebar == 'left') && ($post_sidebar == 'show') ) :
	get_sidebar();
endif;

while ( have_posts() ) : the_post();

	get_template_part( 'template-parts/content/content', 'single' );

	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;

endwhile;

if ( ($sidebar == 'right') && ($post_sidebar == 'show') ) :
	get_sidebar();
endif;

get_footer();

?>