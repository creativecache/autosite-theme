(function( $ ) {
	wp.customize.bind( 'ready', function() {
		
		// Container width
		wp.customize( 'autosite_container_type', function( setting ) {
			wp.customize.control( 'autosite_container_width', function( control ) {
				var visibility = function() {
					if ( 'container-full' !== setting.get() ) {
						control.container.slideDown( 180 );
					} else {
						control.container.slideUp( 180 );
					}
				};
				visibility();
				setting.bind( visibility );
			});
		});

		// Navigation background color
		wp.customize( 'autosite_navigation_position', function( setting ) {
			wp.customize.control( 'autosite_main_nav_bgcolor', function( control ) {
				var visibility = function() {
					if ( 'bottom' == setting.get() || 'centered' == setting.get() ) {
						control.container.slideDown( 180 );
					} else {
						control.container.slideUp( 180 );
					}
				};
				visibility();
				setting.bind( visibility );
			});
		});

		// Toggle tagline option
		wp.customize( 'blogdescription', function( setting ) {
			wp.customize.control( 'autosite_show_tagline', function( control ) {
				var visibility = function() {
					if ( '' !== setting.get() ) {
						control.container.slideDown( 180 );
					} else {
						control.container.slideUp( 180 );
					}
				};
				visibility();
				setting.bind( visibility );
			});
		});

		// Sidebar options
		wp.customize( 'autosite_sidebar_position', function( setting ) {
			wp.customize.control( 'autosite_post_sidebar', function( control ) {
				var visibility = function() {
					if ( 'none' !== setting.get() ) {
						control.container.slideDown( 180 );
					} else {
						control.container.slideUp( 180 );
					}
				};
				visibility();
				setting.bind( visibility );
			});
		});

	});

})( jQuery );