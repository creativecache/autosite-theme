jQuery(document).ready( function($) {

	var sitePadding = '5.62vw';

	function topSection() {
		if ($( 'header' ).hasClass( 'fixed' )) {
			$( 'main > .wrapper > *:first-child' ).css( 'padding-top', 'calc(' + sitePadding + ' * 2 + ' + sitePadding + ')' )
		}
	}

	$( '.sub-menu .menu-item-has-children > a' ).click( function(e) {
		e.preventDefault();
		$(this).next().stop().slideToggle( 400 );
	});

	$( '#main-menu > .menu-item-has-children > a' ).click( function(e) {
		e.preventDefault();
		$( '#main-menu > .menu-item-has-children > a' ).not(this).next().stop().slideUp(400);
		$(this).next().stop().slideToggle(400);

	});

	$( 'main' ).click(function() {
		$( '.menu-item-has-children > a' ).next().stop().slideUp(400);
	});

	topSection();
});