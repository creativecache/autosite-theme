<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;


add_action( 'widgets_init', 'autosite_widgets_init' );
function autosite_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'autosite' ),
        'id' => 'main-sidebar',
        'description' => __( 'Widgets in this area will be shown in the main sidebar.', 'autosite' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h4 class="widgettitle">',
		'after_title'   => '</h4>',
    ) );
}