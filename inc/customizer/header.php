<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'autosite_customize_header_register' ) ) {

	function autosite_customize_header_register( $wp_customize ) {

	/* --------------------
	HEADER SECTION
	-------------------- */

		// Header background color
		$wp_customize->add_setting(
			'autosite_header_bgcolor',
			array(
				'default'           => 'primary',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_header_bgcolor',
				array(
					'label'       => __( 'Header Background Color', 'autosite' ),
					'description' => __( 'Choose the background color for your header.', 'autosite' ),
					'section'     => 'autosite_header_options',
					'settings'    => 'autosite_header_bgcolor',
					'type'        => 'select',
					'choices'     => array(
						'primary'       => __( 'Primary color', 'autosite' ),
						'secondary' 	=> __( 'Secondary color', 'autosite' ),
						'tertiary'      => __( 'Tertiary color', 'autosite' ),
						'white'      => __( 'White', 'autosite' ),
						'black'      => __( 'Black', 'autosite' ),
						'transparent'      => __( 'Transparent', 'autosite' ),
					),
					'priority'    => 10,
				)
			)
		);

		// Header text color
		$wp_customize->add_setting(
			'autosite_header_color',
			array(
				'default'           => 'white',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_header_color',
				array(
					'label'       => __( 'Header Text Color', 'autosite' ),
					'description' => __( 'Choose your header text color.', 'autosite' ),
					'section'     => 'autosite_header_options',
					'settings'    => 'autosite_header_color',
					'type'        => 'select',
					'choices'     => array(
						'primary'       => __( 'Primary color', 'autosite' ),
						'secondary' 	=> __( 'Secondary color', 'autosite' ),
						'tertiary'      => __( 'Tertiary color', 'autosite' ),
						'white'      => __( 'White', 'autosite' ),
						'black'      => __( 'Black', 'autosite' ),
					),
					'priority'    => 15,
				)
			)
		);

		// Header text size
		$wp_customize->add_setting(
			'autosite_header_size',
			array(
				'default'           => 'lg',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_header_size',
				array(
					'label'       => __( 'Header Text Size', 'autosite' ),
					'description' => __( 'Choose your header text size.', 'autosite' ),
					'section'     => 'autosite_header_options',
					'settings'    => 'autosite_header_size',
					'type'        => 'select',
					'choices'     => array(
						'sm'       => __( 'Small', 'autosite' ),
						'md' 	=> __( 'Medium', 'autosite' ),
						'lg'      => __( 'Large', 'autosite' ),
						'xl'      => __( 'Extra Large', 'autosite' ),
					),
					'priority'    => 20,
				)
			)
		);

		// Toggle tagline
		$wp_customize->add_setting(
			'autosite_show_tagline',
			array(
				'default'           => 'hide',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_show_tagline',
				array(
					'label'       => __( 'Show Tagline', 'autosite' ),
					'description' => __( 'Show or hide your tagline.', 'autosite' ),
					'section'     => 'autosite_header_options',
					'settings'    => 'autosite_show_tagline',
					'type'        => 'select',
					'choices'     => array(
						'hide'       => __( 'Hide', 'autosite' ),
						'below' 	=> __( 'Below title', 'autosite' ),
						'right'      => __( 'Next to title', 'autosite' ),
					),
					'priority'    => 25,
				)
			)
		);

		// Header padding
		$wp_customize->add_setting(
			'autosite_header_padding',
			array(
				'default'           => 'md',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_header_padding',
				array(
					'label'       => __( 'Header Top & Bottom Spacing', 'autosite' ),
					'description' => __( 'Choose your header top and bottom spacing.', 'autosite' ),
					'section'     => 'autosite_header_options',
					'settings'    => 'autosite_header_padding',
					'type'        => 'select',
					'choices'     => array(
						'no'       	=> __( 'None', 'autosite' ),
						'xs' 		=> __( 'Extra small', 'autosite' ),
						'sm' 		=> __( 'Small', 'autosite' ),
						'md' 		=> __( 'Medium', 'autosite' ),
						'lg' 		=> __( 'Large', 'autosite' ),
						'xl' 		=> __( 'Extra large', 'autosite' ),
					),
					'priority'    => 30,
				)
			)
		);

		// Fixed header
		$wp_customize->add_setting(
			'autosite_fixed_header',
			array(
				'default'           => 'no',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_radio',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_fixed_header',
				array(
					'label'       => __( 'Fixed Header', 'autosite' ),
					'description' => __( 'Choose if your header stays fixed to the top of the page.', 'autosite' ),
					'section'     => 'autosite_header_options',
					'settings'    => 'autosite_fixed_header',
					'type'        => 'radio',
					'choices'     => array(
						'no'       	=> __( 'Not fixed', 'autosite' ),
						'fixed' 		=> __( 'Fixed', 'autosite' ),
					),
					'priority'    => 35,
				)
			)
		);

	/* --------------------
	   NAVIGATION SECTION
	-------------------- */

		// Navigation position
		$wp_customize->add_setting(
			'autosite_navigation_position',
			array(
				'default'           => 'right',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_navigation_position',
				array(
					'label'       => __( 'Main Navigation Position', 'autosite' ),
					'description' => __( 'Choose your navigation placement.', 'autosite' ),
					'section'     => 'autosite_navigation_options',
					'settings'    => 'autosite_navigation_position',
					'type'        => 'select',
					'choices'     => array(
						'right'		=> __( 'Right header', 'autosite' ),
						'bottom' 	=> __( 'Bottom of header', 'autosite' ),
						'left' 		=> __( 'Left of header', 'autosite' ),
					),
					'priority'    => 10,
				)
			)
		);
		

		if ( ascm_fs()->is__premium_only() ) { 
			if ( ascm_fs()->can_use_premium_code() ) {
				$wp_customize->add_control(
					new WP_Customize_Control(
						$wp_customize,
						'autosite_navigation_position',
						array(
							'label'       => __( 'Main Navigation Position', 'autosite' ),
							'description' => __( 'Choose your navigation placement.', 'autosite' ),
							'section'     => 'autosite_navigation_options',
							'settings'    => 'autosite_navigation_position',
							'type'        => 'select',
							'choices'     => array(
								'right'		=> __( 'Right header', 'autosite' ),
								'bottom' 	=> __( 'Bottom of header', 'autosite' ),
								'left' 		=> __( 'Left of header', 'autosite' ),
								'centered' 	=> __( 'Centered bottom of header', 'autosite' ),
								'split' 	=> __( 'Left and right of header', 'autosite' ),
								'overlay'   => __( 'Fullscreen overlay', 'autosite' ),
							),
							'priority'    => 10,
						)
					)
				);
			} 
		}

		// Main navigation background color
		$wp_customize->add_setting(
			'autosite_main_nav_bgcolor',
			array(
				'default'           => 'secondary',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_main_nav_bgcolor',
				array(
					'label'       => __( 'Main Navigation Background Color', 'autosite' ),
					'description' => __( 'Choose the background color of your main navigation.', 'autosite' ),
					'section'     => 'autosite_navigation_options',
					'settings'    => 'autosite_main_nav_bgcolor',
					'type'        => 'select',
					'choices'     => array(
						'primary'       => __( 'Primary color', 'autosite' ),
						'secondary' 	=> __( 'Secondary color', 'autosite' ),
						'tertiary'      => __( 'Tertiary color', 'autosite' ),
						'white'      => __( 'White', 'autosite' ),
						'black'      => __( 'Black', 'autosite' ),
					),
					'priority'    => 15,
				)
			)
		);

		// Main navigation text color
		$wp_customize->add_setting(
			'autosite_main_nav_color',
			array(
				'default'           => 'white',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_main_nav_color',
				array(
					'label'       => __( 'Main Navigation Text Color', 'autosite' ),
					'description' => __( 'Choose your main navigation text color.', 'autosite' ),
					'section'     => 'autosite_navigation_options',
					'settings'    => 'autosite_main_nav_color',
					'type'        => 'select',
					'choices'     => array(
						'primary'       => __( 'Primary color', 'autosite' ),
						'secondary' 	=> __( 'Secondary color', 'autosite' ),
						'tertiary'      => __( 'Tertiary color', 'autosite' ),
						'white'      => __( 'White', 'autosite' ),
						'black'      => __( 'Black', 'autosite' ),
					),
					'priority'    => 20,
				)
			)
		);

		// Main navigation text transform
		$wp_customize->add_setting(
			'autosite_main_nav_font_weight',
			array(
				'default'           => 'normal',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_main_nav_font_weight',
				array(
					'label'       => __( 'Main Navigation Text Style', 'autosite' ),
					'description' => __( 'Choose font weights for your main navigation. Please note that not all fonts support all weights.', 'autosite' ),
					'section'     => 'autosite_navigation_options',
					'settings'    => 'autosite_main_nav_font_weight',
					'type'        => 'select',
					'choices'     => array(
						'normal'  => __( 'Normal', 'autosite' ),
						'bold' 	  => __( 'Bold', 'autosite' ),
						'italic'  => __( 'Italic', 'autosite' ),
					),
					'priority'    => 25,
				)
			)
		);

		// Main navigation text transform
		$wp_customize->add_setting(
			'autosite_main_nav_text_transform',
			array(
				'default'           => 'none',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_main_nav_text_transform',
				array(
					'label'       => __( 'Main Navigation Text Transform', 'autosite' ),
					'description' => __( 'Choose default text cases for your main navigation.', 'autosite' ),
					'section'     => 'autosite_navigation_options',
					'settings'    => 'autosite_main_nav_text_transform',
					'type'        => 'select',
					'choices'     => array(
						'none'       => __( 'None', 'autosite' ),
						'capitalize' => __( 'Capitalize', 'autosite' ),
						'lowercase'  => __( 'Lowercase', 'autosite' ),
						'uppercase'  => __( 'Uppercase', 'autosite' ),
					),
					'priority'    => 30,
				)
			)
		);

		// Mobile menu button
		$wp_customize->add_setting(
			'autosite_menu_button',
			array(
				'default'           => 'bars',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_menu_button',
				array(
					'label'       => __( 'Menu Toggle Button', 'autosite' ),
					'description' => __( 'Choose the icon for your menu toggle button. Displays on mobile and for certain navigation positions.', 'autosite' ),
					'section'     => 'autosite_navigation_options',
					'settings'    => 'autosite_menu_button',
					'type'        => 'select',
					'choices'     => array(
						'bars'       => __( 'Bars', 'autosite' ),
						'ellipsis-v' => __( 'Vertical Ellipsis', 'autosite' ),
						'ellipsis-h'  => __( 'Horizontal Ellipsis', 'autosite' ),
						'grip-lines'  => __( 'Grip Lines', 'autosite' ),
						'stream'  => __( 'Stream', 'autosite' ),
					),
					'priority'    => 35,
				)
			)
		);

	}

	add_action( 'customize_register', 'autosite_customize_header_register' );

}
