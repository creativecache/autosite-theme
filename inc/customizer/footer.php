<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'autosite_customize_footer_register' ) ) {

	function autosite_customize_footer_register( $wp_customize ) {

	/* --------------------
	FOOTER SECTION
	-------------------- */

	// Footer background color
		$wp_customize->add_setting(
			'autosite_footer_bgcolor',
			array(
				'default'           => 'primary',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_footer_bgcolor',
				array(
					'label'       => __( 'Footer Background Color', 'autosite' ),
					'description' => __( 'Choose the background color for your footer.', 'autosite' ),
					'section'     => 'autosite_footer_options',
					'settings'    => 'autosite_footer_bgcolor',
					'type'        => 'select',
					'choices'     => array(
						'primary'       => __( 'Primary color', 'autosite' ),
						'secondary' 	=> __( 'Secondary color', 'autosite' ),
						'tertiary'      => __( 'Tertiary color', 'autosite' ),
						'white'      => __( 'White', 'autosite' ),
						'black'      => __( 'Black', 'autosite' ),
					),
					'priority'    => 10,
				)
			)
		);

	// Footer text color
		$wp_customize->add_setting(
			'autosite_footer_color',
			array(
				'default'           => 'primary',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_footer_color',
				array(
					'label'       => __( 'Footer Text Color', 'autosite' ),
					'description' => __( 'Choose the text color for your footer.', 'autosite' ),
					'section'     => 'autosite_footer_options',
					'settings'    => 'autosite_footer_color',
					'type'        => 'select',
					'choices'     => array(
						'primary'       => __( 'Primary color', 'autosite' ),
						'secondary' 	=> __( 'Secondary color', 'autosite' ),
						'tertiary'      => __( 'Tertiary color', 'autosite' ),
						'white'      => __( 'White', 'autosite' ),
						'black'      => __( 'Black', 'autosite' ),
					),
					'priority'    => 15,
				)
			)
		);

	// Footer link color
		$wp_customize->add_setting(
			'autosite_footer_link_color',
			array(
				'default'           => 'primary',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_footer_link_color',
				array(
					'label'       => __( 'Footer Link Color', 'autosite' ),
					'description' => __( 'Choose the color for your footer links.', 'autosite' ),
					'section'     => 'autosite_footer_options',
					'settings'    => 'autosite_footer_link_color',
					'type'        => 'select',
					'choices'     => array(
						'primary'       => __( 'Primary color', 'autosite' ),
						'secondary' 	=> __( 'Secondary color', 'autosite' ),
						'tertiary'      => __( 'Tertiary color', 'autosite' ),
						'white'      => __( 'White', 'autosite' ),
						'black'      => __( 'Black', 'autosite' ),
					),
					'priority'    => 20,
				)
			)
		);

	}

	add_action( 'customize_register', 'autosite_customize_footer_register' );
}