<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'autosite_customize_layout_register' ) ) {

	function autosite_customize_layout_register( $wp_customize ) {
		
		// Container type
		$wp_customize->add_setting(
			'autosite_container_type',
			array(
				'default'           => 'container',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_container_type',
				array(
					'label'       => __( 'Container Width', 'autosite' ),
					'description' => __( 'Choose between a fixed or full width layout', 'autosite' ),
					'section'     => 'autosite_layout_options',
					'settings'    => 'autosite_container_type',
					'type'        => 'select',
					'choices'     => array(
						'container'       => __( 'Fixed width container', 'autosite' ),
						'container-full' => __( 'Full width container', 'autosite' ),
					),
					'priority'    => 10,
				)
			)
		);



		// Fixed container width
		$wp_customize->add_setting(
			'autosite_container_width',
			array(
				'default'           => 'large',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_container_width',
				array(
					'label'       => __( 'Fixed Container Width', 'autosite' ),
					'description' => __( 'Choose the width of your fixed container.', 'autosite' ),
					'section'     => 'autosite_layout_options',
					'settings'    => 'autosite_container_width',
					'type'        => 'select',
					'choices'     => array(
						'sm'       	=> __( 'Small', 'autosite' ),
						'md' 		=> __( 'Medium', 'autosite' ),
						'lg' 		=> __( 'Large', 'autosite' ),
						'xl' 		=> __( 'Extra Large', 'autosite' ),
					),
					'priority'    => 15,
				)
			)
		);

		// Sidebar position
		$wp_customize->add_setting(
			'autosite_sidebar_position',
			array(
				'default'           => 'right',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_sidebar_position',
				array(
					'label'       => __( 'Sidebar Position', 'autosite' ),
					'description' => __( 'Choose the location of your sidebar.', 'autosite' ),
					'section'     => 'autosite_layout_options',
					'settings'    => 'autosite_sidebar_position',
					'type'        => 'select',
					'choices'     => array(
						'none'       	=> __( 'No Sidebar', 'autosite' ),
						'left' 		=> __( 'Left', 'autosite' ),
						'right' 		=> __( 'Right', 'autosite' ),
					),
					'priority'    => 20,
				)
			)
		);

	}

	add_action( 'customize_register', 'autosite_customize_layout_register' );

}
