<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'autosite_customize_styles_register' ) ) {

	function autosite_customize_styles_register( $wp_customize ) {

		class Google_Font_Dropdown_Custom_Control extends WP_Customize_Control{
			private $fonts = false;
		    public function __construct($manager, $id, $args = array(), $options = array()){
		        $this->fonts = $this->get_google_fonts();
		        parent::__construct( $manager, $id, $args );
		    }

		    public function render_content(){
		        ?>
		            <label class="customize_dropdown_input">
		                <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
		            	<select id="<?php echo esc_attr($this->id); ?>" name="<?php echo esc_attr($this->id); ?>" data-customize-setting-link="<?php echo esc_attr($this->id); ?>">
		                    <?php
		                        foreach ( $this->fonts as $k => $v ){
		                            echo '<option value="'.$v['family'].'" ' . selected( $this->value(), $v['family'], false ) . '>'.$v['family'].'</option>';
		                        }
		                    ?>
		                </select>
		            </label>
		        <?php
		    }

			public function get_google_fonts(){
				if (get_transient('autosite_google_font_list_updated')) {
		        	$content = get_transient('autosite_google_font_list_updated');
			    } else {
			        $googleApi = 'https://www.googleapis.com/webfonts/v1/webfonts?sort=alpha&key=AIzaSyDw4v206altgI98UuH4xXSPiObEBsfa1Ko';
			        $fontContent = wp_remote_get( $googleApi, array('sslverify'   => true) );
			        $content = json_decode($fontContent['body'], true);
			        set_transient( 'autosite_google_font_list_updated', $content, 0 );
			    }

			    return $content['items'];
			}

		}

		/* --------------------
		COLORS SECTION
		-------------------- */

		// Primary color
		$wp_customize->add_setting(
			'autosite_primary_color',
			array(
				'default'           => '#390099',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Color_Control(
				$wp_customize,
				'autosite_primary_color',
				array(
					'label'       => __( 'Primary Color', 'autosite' ),
					'description' => __( 'Choose the main color for your website.', 'autosite' ),
					'section'     => 'autosite_colors_options',
					'settings'    => 'autosite_primary_color',
					'priority'    => 10,
				)
			)
		);

		// Secondary color
		$wp_customize->add_setting(
			'autosite_secondary_color',
			array(
				'default'           => '#3DDC97',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Color_Control(
				$wp_customize,
				'autosite_secondary_color',
				array(
					'label'       => __( 'Secondary Color', 'autosite' ),
					'description' => __( 'Choose the secondary color for your website.', 'autosite' ),
					'section'     => 'autosite_colors_options',
					'settings'    => 'autosite_secondary_color',
					'priority'    => 15,
				)
			)
		);

		// Tertiary color
		$wp_customize->add_setting(
			'autosite_tertiary_color',
			array(
				'default'           => '#DFDCE5',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Color_Control(
				$wp_customize,
				'autosite_tertiary_color',
				array(
					'label'       => __( 'Tertiary Color', 'autosite' ),
					'description' => __( 'Choose the tertiary color for your website.', 'autosite' ),
					'section'     => 'autosite_colors_options',
					'settings'    => 'autosite_tertiary_color',
					'priority'    => 20,
				)
			)
		);

		// Primary background text color
		$wp_customize->add_setting(
			'autosite_primarybg_text_color',
			array(
				'default'           => 'white',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_primarybg_text_color',
				array(
					'label'       => __( 'Primary Background Text Color', 'autosite' ),
					'description' => __( 'Choose the default text color for text on your primary color background.', 'autosite' ),
					'section'     => 'autosite_colors_options',
					'settings'    => 'autosite_primarybg_text_color',
					'type'        => 'select',
					'choices'     => array(
						'secondary' 	=> __( 'Secondary color', 'autosite' ),
						'tertiary'      => __( 'Tertiary color', 'autosite' ),
						'white'      => __( 'White', 'autosite' ),
						'black'      => __( 'Black', 'autosite' ),
					),

					'priority'    => 25,
				)
			)
		);

		// Primary background link color
		$wp_customize->add_setting(
			'autosite_primarybg_link_color',
			array(
				'default'           => 'secondary',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_primarybg_link_color',
				array(
					'label'       => __( 'Primary Background Link Color', 'autosite' ),
					'description' => __( 'Choose the default link color for links on your primary color background.', 'autosite' ),
					'section'     => 'autosite_colors_options',
					'settings'    => 'autosite_primarybg_link_color',
					'type'        => 'select',
					'choices'     => array(
						'secondary' 	=> __( 'Secondary color', 'autosite' ),
						'tertiary'      => __( 'Tertiary color', 'autosite' ),
						'white'      => __( 'White', 'autosite' ),
						'black'      => __( 'Black', 'autosite' ),
					),

					'priority'    => 30,
				)
			)
		);

		// Secondary background text color
		$wp_customize->add_setting(
			'autosite_secondarybg_text_color',
			array(
				'default'           => 'black',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_secondarybg_text_color',
				array(
					'label'       => __( 'Secondary Background Text Color', 'autosite' ),
					'description' => __( 'Choose the default text color for text on your secondary color background.', 'autosite' ),
					'section'     => 'autosite_colors_options',
					'settings'    => 'autosite_secondarybg_text_color',
					'type'        => 'select',
					'choices'     => array(
						'primary' 	=> __( 'Primary color', 'autosite' ),
						'tertiary'      => __( 'Tertiary color', 'autosite' ),
						'white'      => __( 'White', 'autosite' ),
						'black'      => __( 'Black', 'autosite' ),
					),

					'priority'    => 35,
				)
			)
		);

		// Secondary background link color
		$wp_customize->add_setting(
			'autosite_secondarybg_link_color',
			array(
				'default'           => 'primary',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_secondarybg_link_color',
				array(
					'label'       => __( 'Secondary Background Link Color', 'autosite' ),
					'description' => __( 'Choose the default link color for links on your secondary color background.', 'autosite' ),
					'section'     => 'autosite_colors_options',
					'settings'    => 'autosite_secondarybg_link_color',
					'type'        => 'select',
					'choices'     => array(
						'primary' 	=> __( 'Primary color', 'autosite' ),
						'tertiary'      => __( 'Tertiary color', 'autosite' ),
						'white'      => __( 'White', 'autosite' ),
						'black'      => __( 'Black', 'autosite' ),
					),

					'priority'    => 40,
				)
			)
		);

		// Tertiary background text color
		$wp_customize->add_setting(
			'autosite_tertiarybg_text_color',
			array(
				'default'           => 'black',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_tertiarybg_text_color',
				array(
					'label'       => __( 'Tertiary Background Text Color', 'autosite' ),
					'description' => __( 'Choose the default text color for text on your tertiary color background.', 'autosite' ),
					'section'     => 'autosite_colors_options',
					'settings'    => 'autosite_tertiarybg_text_color',
					'type'        => 'select',
					'choices'     => array(
						'primary' 	=> __( 'Primary color', 'autosite' ),
						'secondary'      => __( 'Secondary color', 'autosite' ),
						'white'      => __( 'White', 'autosite' ),
						'black'      => __( 'Black', 'autosite' ),
					),

					'priority'    => 45,
				)
			)
		);

		// Tertiary background link color
		$wp_customize->add_setting(
			'autosite_tertiarybg_link_color',
			array(
				'default'           => 'primary',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_tertiarybg_link_color',
				array(
					'label'       => __( 'Tertiary Background Link Color', 'autosite' ),
					'description' => __( 'Choose the default link color for links on your tertiary color background.', 'autosite' ),
					'section'     => 'autosite_colors_options',
					'settings'    => 'autosite_tertiarybg_link_color',
					'type'        => 'select',
					'choices'     => array(
						'primary' 	=> __( 'Primary color', 'autosite' ),
						'secondary'      => __( 'Secondary color', 'autosite' ),
						'white'      => __( 'White', 'autosite' ),
						'black'      => __( 'Black', 'autosite' ),
					),

					'priority'    => 50,
				)
			)
		);

		/* --------------------
		FONTS SECTION
		-------------------- */

		// Heading font
		$wp_customize->add_setting(
			'autosite_heading_font',
			array(
				'default'           => '',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
			)
		);

		$wp_customize->add_control(
			new Google_Font_Dropdown_Custom_Control(
				$wp_customize,
				'autosite_heading_font',
				array(
					'label'       => __( 'Heading Font', 'autosite' ),
					'description' => __( 'Choose the font for all headings on your website.', 'autosite' ),
					'section'     => 'autosite_font_options',
					'settings'    => 'autosite_heading_font',
					'priority'    => 10,
				)
			)
		);

		// Body font
		$wp_customize->add_setting(
			'autosite_body_font',
			array(
				'default'           => '',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
			)
		);

		$wp_customize->add_control(
			new Google_Font_Dropdown_Custom_Control(
				$wp_customize,
				'autosite_body_font',
				array(
					'label'       => __( 'Body Font', 'autosite' ),
					'description' => __( 'Choose the font for the rest of the text on your website.', 'autosite' ),
					'section'     => 'autosite_font_options',
					'settings'    => 'autosite_body_font',
					'priority'    => 15,
				)
			)
		);

	}

	add_action( 'customize_register', 'autosite_customize_styles_register' );
}

if ( ! function_exists( 'autosite_fonts_url' ) ) {
	function autosite_fonts_url() {
	    $fonts_url = '';
	    $heading_font = get_theme_mod('autosite_heading_font', '');
	    $body_font = get_theme_mod('autosite_body_font', '');
	    // Translators: If there are characters in your language that are not supported by Google font, translate it to 'off'. Do not translate into your own language.
	    // $heading_font = _x( 'on', ''.$heading_font.' font: on or off', 'yourtheme' );


	    if ( 'off' !== $heading_font || 'off' !== $body_font ) {
	        $font_families = array();

	        if ( 'off' !== $heading_font ) {
	            $font_families[] = $heading_font;
	        }

	        if ( 'off' !== $body_font ) {
	            $font_families[] = $body_font;
	        }

	        $query_args = array(
	            'family' => urlencode( implode( '|', array_unique($font_families) ) ),
	        );

	        $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	    }

	    return esc_url_raw( $fonts_url );
	}
}

