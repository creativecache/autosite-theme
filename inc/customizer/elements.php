<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'autosite_customize_elements_register' ) ) {

	function autosite_customize_elements_register( $wp_customize ) {

		// Link color
		$wp_customize->add_setting(
			'autosite_link_element_color',
			array(
				'default'           => 'primary',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_select',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_link_element_color',
				array(
					'label'       => __( 'Link Color', 'autosite' ),
					'description' => __( 'Choose the default color for your link elements.', 'autosite' ),
					'section'     => 'autosite_link_element_options',
					'settings'    => 'autosite_link_element_color',
					'type'        => 'select',
					'choices'     => array(
						'primary'      	=> __( 'Primary color', 'autosite' ),
						'secondary' 	=> __( 'Secondary color', 'autosite' ),
						'tertiary'      => __( 'Tertiary color', 'autosite' ),
						'white'      	=> __( 'White', 'autosite' ),
						'black'      	=> __( 'Black', 'autosite' ),
					),
					'priority'    => 10,
				)
			)
		);

	}

	add_action( 'customize_register', 'autosite_customize_elements_register' );

}