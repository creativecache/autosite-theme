<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'autosite_customize_advanced_register' ) ) {

	function autosite_customize_advanced_register( $wp_customize ) {

		$wp_customize->add_section(
			'autosite_advanced_options',
			array(
				'title'       => __( 'Advanced', 'autosite' ),
				'capability'  => 'edit_theme_options',
				'description' => __( 'Modify advanced options', 'autosite' ),
				'priority'    => 160,
			)
		);

		// Google Analytics
		$wp_customize->add_setting(
			'autosite_google_analytics',
			array(
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'wp_filter_nohtml_kses',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_google_analytics',
				array(
					'label'       => __( 'Google Analytics', 'autosite' ),
					'description' => __( 'Add your Google Analytics tracking code ID below. For proper use please only include the numbers after "UA-". Leave blank to remove Google Analytics functionality.', 'autosite' ),
					'section'     => 'autosite_advanced_options',
					'settings'    => 'autosite_google_analytics',
					'type'        => 'text',
					'priority'    => 10,
				)
			)
		);

	}

	add_action( 'customize_register', 'autosite_customize_advanced_register' );

}
