<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'autosite_customize_pages_register' ) ) {

	function autosite_customize_pages_register( $wp_customize ) {

	/* --------------------
	POSTS SECTION
	-------------------- */

		// Toggle post sidebar
		$wp_customize->add_setting(
			'autosite_post_sidebar',
			array(
				'default'           => 'show',
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'autosite_sanitize_radio',
			)
		);

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'autosite_post_sidebar',
				array(
					'label'       => __( 'Toggle Sidebar', 'autosite' ),
					'description' => __( 'Choose to show or hide your sidebar on your posts.', 'autosite' ),
					'section'     => 'autosite_post_options',
					'settings'    => 'autosite_post_sidebar',
					'type'        => 'radio',
					'choices'     => array(
						'show'       => __( 'Show', 'autosite' ),
						'hide' 	=> __( 'Hide', 'autosite' ),
					),
					'priority'    => 10,
				)
			)
		);

	/* --------------------
	PAGES SECTION
	-------------------- */



	/* --------------------
	ARCHIVES SECTION
	-------------------- */
		


	}

	add_action( 'customize_register', 'autosite_customize_pages_register' );

}
