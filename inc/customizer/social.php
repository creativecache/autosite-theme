<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'autosite_customize_social_register' ) ) {

	function autosite_customize_social_register( $wp_customize ) {

		/* --------------------
		SOCIAL LINKS SECTION
		-------------------- */

			// Facebook
			$wp_customize->add_setting(
				'autosite_social_facebook',
				array(
					'default'           => '',
					'type'              => 'theme_mod',
					'capability'        => 'edit_theme_options',
					'sanitize_callback' => 'esc_url_raw',
				)
			);

			$wp_customize->add_control(
				new WP_Customize_Control(
					$wp_customize,
					'autosite_social_facebook',
					array(
						'label'       => __( 'Facebook URL', 'autosite' ),
						'section'     => 'autosite_social_links',
						'settings'    => 'autosite_social_facebook',
						'type'        => 'url',
						'priority'    => 10,
					)
				)
			);

			// Instagram
			$wp_customize->add_setting(
				'autosite_social_instagram',
				array(
					'default'           => '',
					'type'              => 'theme_mod',
					'capability'        => 'edit_theme_options',
					'sanitize_callback' => 'esc_url_raw',
				)
			);

			$wp_customize->add_control(
				new WP_Customize_Control(
					$wp_customize,
					'autosite_social_instagram',
					array(
						'label'       => __( 'Instagram URL', 'autosite' ),
						'section'     => 'autosite_social_links',
						'settings'    => 'autosite_social_instagram',
						'type'        => 'url',
						'priority'    => 15,
					)
				)
			);

			// Twitter
			$wp_customize->add_setting(
				'autosite_social_twitter',
				array(
					'default'           => '',
					'type'              => 'theme_mod',
					'capability'        => 'edit_theme_options',
					'sanitize_callback' => 'esc_url_raw',
				)
			);

			$wp_customize->add_control(
				new WP_Customize_Control(
					$wp_customize,
					'autosite_social_twitter',
					array(
						'label'       => __( 'Twitter URL', 'autosite' ),
						'section'     => 'autosite_social_links',
						'settings'    => 'autosite_social_twitter',
						'type'        => 'url',
						'priority'    => 20,
					)
				)
			);

			// LinkedIn
			$wp_customize->add_setting(
				'autosite_social_linkedin',
				array(
					'default'           => '',
					'type'              => 'theme_mod',
					'capability'        => 'edit_theme_options',
					'sanitize_callback' => 'esc_url_raw',
				)
			);

			$wp_customize->add_control(
				new WP_Customize_Control(
					$wp_customize,
					'autosite_social_linkedin',
					array(
						'label'       => __( 'LinkedIn URL', 'autosite' ),
						'section'     => 'autosite_social_links',
						'settings'    => 'autosite_social_linkedin',
						'type'        => 'url',
						'priority'    => 25,
					)
				)
			);

		/* --------------------
		SOCIAL ICONS SECTION
		-------------------- */



		/* --------------------
		SOCIAL PLACEMENT SECTION
		-------------------- */


	}

	add_action( 'customize_register', 'autosite_customize_social_register' );

}
