<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;



function autosite_customize_admin_register( $wp_customize ) {

	$wp_customize->remove_control( 'custom_css' );
}

add_action( 'customize_register', 'autosite_customize_admin_register' );