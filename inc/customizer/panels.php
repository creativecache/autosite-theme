<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'autosite_customizer_panels_register' ) ) {

	function autosite_customizer_panels_register( $wp_customize ) {

	/* --------------------
	25 - STYLE OPTIONS
	-------------------- */

		$wp_customize->add_panel(
			'autosite_styles_panel',
			array(
			    'title'			=>'Styles',
			    'description'	=> 'These are all of the styles for your website.',
			    'capability'  	=> 'edit_theme_options',
			    'priority'		=> 25,
			)
		);

		// Colors Section
		$wp_customize->add_section(
			'autosite_colors_options',
			array(
				'title'       	=> __( 'Colors', 'autosite' ),
				'capability'  	=> 'edit_theme_options',
				'description' 	=> __( 'This is the color scheme for your website.', 'autosite' ),
				'panel'			=>'autosite_styles_panel',
				'priority'    	=> 10,
			)
		);

		// Fonts Section
		$wp_customize->add_section(
			'autosite_font_options',
			array(
				'title'       	=> __( 'Fonts', 'autosite' ),
				'capability'  	=> 'edit_theme_options',
				'description' 	=> __( 'These are the fonts for your website.', 'autosite' ),
				'panel'			=>'autosite_styles_panel',
				'priority'    	=> 15,
			)
		);

	/* --------------------
	27 - ELEMENT OPTIONS
	-------------------- */

		$wp_customize->add_panel(
			'autosite_elements_panel',
			array(
			    'title'			=> 'Elements',
			    'description'	=> 'These are all of the basic elements for your website.',
			    'capability'  	=> 'edit_theme_options',
			    'priority'		=> 27,
			)
		);

		// Link Element Section
		$wp_customize->add_section(
			'autosite_link_element_options',
			array(
				'title'       	=> __( 'Links', 'autosite' ),
				'capability'  	=> 'edit_theme_options',
				'description' 	=> __( 'These are the options for links on your website.', 'autosite' ),
				'panel'			=>'autosite_elements_panel',
				'priority'    	=> 10,
			)
		);

	/* --------------------
	30 - LAYOUT OPTIONS
	-------------------- */

		// Layout
		$wp_customize->add_section(
			'autosite_layout_options',
			array(
				'title'      	=> __( 'Layout', 'autosite' ),
				'capability' 	=> 'edit_theme_options',
				'description' 	=> __( 'Choose layout options.', 'autosite' ),
				'priority'    	=> 30,
			)
		);

	/* --------------------
	35 - HEADER OPTIONS
	-------------------- */

		$wp_customize->add_panel(
			'autosite_header_panel',
			array(
			    'title'			=>'Header',
			    'description'	=> 'These are the options for the header of your website.',
			    'capability'  	=> 'edit_theme_options',
			    'priority'		=> 35,
			)
		);

		// Header Section
		$wp_customize->add_section(
			'autosite_header_options',
			array(
				'title'       	=> __( 'Header Basics', 'autosite' ),
				'capability'  	=> 'edit_theme_options',
				'description' 	=> __( 'Choose how the header looks for your website.', 'autosite' ),
				'panel'			=>'autosite_header_panel',
				'priority'    	=> 10,
			)
		);

		// Navigation Section
		$wp_customize->add_section(
			'autosite_navigation_options',
			array(
				'title'       	=> __( 'Navigation', 'autosite' ),
				'capability'  	=> 'edit_theme_options',
				'description' 	=> __( 'Change the navigation settings for your website.', 'autosite' ),
				'panel'			=>'autosite_header_panel',
				'priority'    	=> 15,
			)
		);

	/* --------------------
	40 - PAGE TYPES
	-------------------- */

		$wp_customize->add_panel(
			'autosite_pages_panel',
			array(
			    'title'			=>'Page Types',
			    'description'	=> 'These are the options for all page types on your website.',
			    'capability'  	=> 'edit_theme_options',
			    'priority'		=> 40,
			)
		);

		// Posts Section
		$wp_customize->add_section(
			'autosite_post_options',
			array(
				'title'       	=> __( 'Posts', 'autosite' ),
				'capability'  	=> 'edit_theme_options',
				'description' 	=> __( 'Choose how post pages look on your website.', 'autosite' ),
				'panel'			=>'autosite_pages_panel',
				'priority'    	=> 10,
			)
		);

		// Pages Section
		$wp_customize->add_section(
			'autosite_page_options',
			array(
				'title'       	=> __( 'Pages', 'autosite' ),
				'capability'  	=> 'edit_theme_options',
				'description' 	=> __( 'Choose how regular pages look on your website.', 'autosite' ),
				'panel'			=>'autosite_pages_panel',
				'priority'    	=> 15,
			)
		);

		// Archives Section
		$wp_customize->add_section(
			'autosite_archive_options',
			array(
				'title'       	=> __( 'Archives', 'autosite' ),
				'capability'  	=> 'edit_theme_options',
				'description' 	=> __( 'Choose how archive pages look on your website. These include posts, category, and tag archives. Category and tag archives can be individually edited under their sections.', 'autosite' ),
				'panel'			=>'autosite_pages_panel',
				'priority'    	=> 20,
			)
		);

	/* --------------------
	80 - SOCIAL OPTIONS
	-------------------- */

		$wp_customize->add_panel(
			'autosite_social_panel',
			array(
			    'title'			=>'Social Media',
			    'description'	=> 'These are the social media options for your website.',
			    'capability'  	=> 'edit_theme_options',
			    'priority'		=> 80,
			)
		);

		// Social Links
		$wp_customize->add_section(
			'autosite_social_links',
			array(
				'title'       	=> __( 'Social Links', 'autosite' ),
				'capability'  	=> 'edit_theme_options',
				'description' 	=> __( 'Add links to your social media accounts. Leave blank to remove.', 'autosite' ),
				'panel'			=>'autosite_social_panel',
				'priority'    	=> 10,
			)
		);

		// Social Icons
		$wp_customize->add_section(
			'autosite_social_icons',
			array(
				'title'       	=> __( 'Social Icons', 'autosite' ),
				'capability'  	=> 'edit_theme_options',
				'description' 	=> __( 'Change the look of your social media icons.', 'autosite' ),
				'panel'			=>'autosite_social_panel',
				'priority'    	=> 15,
			)
		);

		// Social Placement
		$wp_customize->add_section(
			'autosite_social_placement',
			array(
				'title'       	=> __( 'Social Placement', 'autosite' ),
				'capability'  	=> 'edit_theme_options',
				'description' 	=> __( 'Change the locations of your social media icons.', 'autosite' ),
				'panel'			=>'autosite_social_panel',
				'priority'    	=> 20,
			)
		);

	/* --------------------
	85 - FOOTER
	-------------------- */

		// Footer
		$wp_customize->add_section(
			'autosite_footer_options',
			array(
				'title'       	=> __( 'Footer', 'autosite' ),
				'capability'  	=> 'edit_theme_options',
				'description' 	=> __( 'Change your website footer.', 'autosite' ),
				'priority'    	=> 85,
			)
		);

	}
}
add_action( 'customize_register', 'autosite_customizer_panels_register' );