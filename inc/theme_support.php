<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

add_theme_support( 'post-thumbnails' );
add_theme_support( 'title-tag' );
add_theme_support( 'custom-logo' );
add_theme_support( 'editor-style' );
add_theme_support( 'automatic-feed-links' );

add_theme_support( 'editor-color-palette', array(
	array(
		'name'  => __( 'Primary', 'autosite' ),
		'slug'  => 'primary',
		'color'	=> get_theme_mod('autosite_primary_color'),
	),

	array(
		'name'  => __( 'Secondary', 'autosite' ),
		'slug'  => 'secondary',
		'color' => get_theme_mod('autosite_secondary_color'),
	),

	array(
		'name'  => __( 'Tertiary', 'autosite' ),
		'slug'  => 'tertiary',
		'color' => get_theme_mod('autosite_tertiary_color'),
	),

	array(
		'name'	=> __( 'White', 'autosite' ),
		'slug'	=> 'white',
		'color'	=> '#ffffff',
	),

	array(
		'name'	=> __( 'Black', 'autosite' ),
		'slug'	=> 'black',
		'color'	=> '#000000',
	),

) );

add_theme_support( 'disable-custom-colors' );