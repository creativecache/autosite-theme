<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$autosite_customizer_includes = array(
	'/admin.php',
	'/panels.php',
	'/styles.php',
	'/elements.php',
	'/layout.php',
	'/header.php',
	'/page-content.php',
	'/social.php',
	'/footer.php',
	'/advanced.php',
);

foreach ( $autosite_customizer_includes as $file ) {
	$filepath = locate_template( 'inc/customizer' . $file );
	if ( ! $filepath ) {
		trigger_error( sprintf( 'Error locating /customizer%s for inclusion', $file ), E_USER_ERROR );
	}
	require_once $filepath;
}