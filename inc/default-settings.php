<?php
/**
 * Check and setup theme's default settings
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$defaults = array(
	'primary_color'				=> '#390099',
	'primarybg_text_color'		=> 'white',
	'primarybg_link_color'		=> 'secondary',
	'secondary_color'			=> '#3DDC97',
	'secondarybg_text_color'	=> 'black',
	'secondarybg_link_color'	=> 'primary',
	'tertiary_color'			=> '#DFDCE5',
	'tertiarybg_text_color'		=> 'black',
	'tertiarybg_link_color'		=> 'primary',
	'heading_font'				=> 'Alice',
	'body_font'					=> 'IBM Plex Sans Condensed',
	'container_type'			=> 'container',
	'container_width'			=> 'lg',
	'show_tagline'				=> 'hide',
	'navigation_position'		=> 'right',
	'header_padding'			=> 'md',
	'header_size'				=> 'lg',
	'sidebar_position'			=> 'right',
	'post_sidebar'				=> 'show',
	'header_bgcolor'			=> 'primary',
	'fixed_header'				=> 'no',
);

foreach ($defaults as $key => $value) {
	$key = 'autosite_' . $key;
	$get_mod = get_theme_mod( $key );
	if ( '' == $get_mod ) {
		set_theme_mod( $key, $value );
	}
}