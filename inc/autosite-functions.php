<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'autosite_copyright' ) ) {

	function autosite_copyright() {
		do_action( 'autosite_copyright' );
	}

}

if ( ! function_exists( 'autosite_copyright_info' ) ) {

	add_action( 'autosite_copyright', 'autosite_copyright_info' );

	function autosite_copyright_info() {
		
		$site_info = sprintf(
			'&copy; %1$s <a href="%2$s" rel="home" itemprop="url">%3$s</a>, all rights reserved | theme by %4$s',
			date('Y'),
			esc_url( __( home_url( '/' ), 'autosite' ) ),
			esc_attr( __( get_bloginfo( 'name', 'display' ), 'autosite' ) ),
			sprintf(
				esc_html__( '%1$s.', 'autosite' ),
				'<a href="' . esc_url( __( 'https://autosite.io', 'autosite' ) ) . '" target="_blank">Autosite.io</a>'
			)
		);

		echo apply_filters( 'autosite_copyright_info_content', $site_info );

	}

}