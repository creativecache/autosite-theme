<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'autosite_scripts' ) ) {

	// Load theme's JavaScript and CSS sources
	function autosite_scripts() {

		// Get the theme data
		$the_theme     = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );

		$css_version = $theme_version . '.' . filemtime( get_template_directory() . '/style.css' );
		wp_enqueue_style( 'font-awesome', 'https://use.fontawesome.com/releases/v5.8.2/css/all.css', array(), $theme_version );
		wp_enqueue_style( 'autosite-google-fonts', autosite_fonts_url(), array(), $theme_version );
		wp_enqueue_style( 'autosite-styles', get_template_directory_uri() . '/style.css', array(), $css_version );

		wp_enqueue_script( 'jquery' );

		$js_version = $theme_version . '.' . filemtime( get_template_directory() . '/js/scripts.js' );
		wp_enqueue_script( 'autosite-scripts', get_template_directory_uri() . '/js/scripts.js', array(), $js_version, true );

		function themeModColors( $handle ) {
			if ('white' == get_theme_mod('autosite_' . $handle) || 'black' == get_theme_mod('autosite_' . $handle) || 'transparent' == get_theme_mod('autosite_' . $handle)) {
				$$handle = get_theme_mod('autosite_' . $handle);
			} else {
				$$handle = get_theme_mod('autosite_' . get_theme_mod('autosite_' . $handle) . '_color');
			}

			return $$handle;
		}

		// Custom CSS
		$custom_css = "
		:root {
			--primary-color: " . get_theme_mod('autosite_primary_color') . ";
			--secondary-color: " . get_theme_mod('autosite_secondary_color') . ";
			--tertiary-color: " . get_theme_mod('autosite_tertiary_color') . ";
		}
		 
		body { font-family: '" . get_theme_mod('autosite_body_font') . "'; }

		h1, h2, h3, h4, h5, h6, #masthead .site-brand { font-family: '" . get_theme_mod('autosite_heading_font') . "'; }

		a { color: " . themeModColors( 'link_element_color' ) . "; }

		.primary-color { color: " . get_theme_mod('autosite_primary_color') . "; }
		.primary-bg, .has-primary-background-color { background-color: " . get_theme_mod('autosite_primary_color') . "; color: " . themeModColors( 'primarybg_text_color' ) . "; }
		.primary-bg a, .has-primary-background-color a { color: " . themeModColors( 'primarybg_link_color' ) . "; }
		.secondary-color { color: " . get_theme_mod('autosite_secondary_color') . "; }
		.secondary-bg, .has-secondary-background-color { background-color: " . get_theme_mod('autosite_secondary_color') . "; color: " . themeModColors( 'secondarybg_text_color' ) . "; }
		.secondary-bg a, .has-secondary-background-color a { color: " . themeModColors( 'secondarybg_link_color' ) . "; }
		.tertiary-color { color: " . get_theme_mod('autosite_tertiary_color') . "; }
		.tertiary-bg, .has-tertiary-background-color { background-color: " . get_theme_mod('autosite_tertiary_color') . "; color: " . themeModColors( 'tertiarybg_text_color' ) . "; }
		.tertiary-bg a, .has-tertiary-background-color a { color: " . themeModColors( 'tertiarybg_link_color' ) . "; }

		#masthead .site-brand { color: " . themeModColors( 'autosite_header_color' ) . " }

		#main-menu li a { color: " . themeModColors( 'main_nav_color' ) . "; text-transform: " . get_theme_mod('autosite_main_nav_text_transform') . "; font-weight: " . get_theme_mod('autosite_main_nav_font_weight') . "; }
		";

		wp_add_inline_style( 'autosite-styles', $custom_css );
	}
	add_action( 'wp_enqueue_scripts', 'autosite_scripts' );
}


if ( ! function_exists( 'autosite_customizer_controls' ) ) {
	function autosite_customizer_controls() {

		// Get the theme data
		$the_theme     = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );
		
		$js_version = $theme_version . '.' . filemtime( get_template_directory() . '/js/customize-scripts.js' );
	    wp_enqueue_script( 'autosite-customizer-controls', get_template_directory_uri() . '/js/customize-scripts.js', array( 'jquery' ), $js_version, true );
	}
	add_action( 'customize_controls_enqueue_scripts', 'autosite_customizer_controls' );
}

if ( ! function_exists( 'autosite_admin_scripts' ) ) {

	function autosite_admin_scripts() {
		
		// Get the theme data
		$the_theme     = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );

		$css_version = $theme_version . '.' . filemtime( get_template_directory() . '/style.css' );
		wp_enqueue_style( 'autosite-admin-styles', get_template_directory_uri() . '/style-editor.css', array(), $css_version );
		wp_enqueue_style( 'autosite-google-fonts', autosite_fonts_url(), array(), $theme_version );

		function themeModColors( $handle ) {
			if ('white' == get_theme_mod('autosite_' . $handle) || 'black' == get_theme_mod('autosite_' . $handle) || 'transparent' == get_theme_mod('autosite_' . $handle)) {
				$$handle = get_theme_mod('autosite_' . $handle);
			} else {
				$$handle = get_theme_mod('autosite_' . get_theme_mod('autosite_' . $handle) . '_color');
			}

			return $$handle;
		}

		// Custom CSS
		$custom_css = "
		.editor-styles-wrapper { font-family: '" . get_theme_mod('autosite_body_font') . "' !important; }

		.editor-styles-wrapper h1, .editor-styles-wrapper h2, .editor-styles-wrapper h3, .editor-styles-wrapper h4, .editor-styles-wrapper h5, .editor-styles-wrapper h, .editor-post-title__block .editor-post-title__input { font-family: '" . get_theme_mod('autosite_heading_font') . "'; }

		.edit-post-visual-editor a { color: " . themeModColors( 'link_element_color' ) . "; }

		.edit-post-visual-editor .has-primary-background-color { color: " . themeModColors( 'primarybg_text_color' ) . "; }
		.edit-post-visual-editor .has-primary-background-color a { color: " . themeModColors( 'primarybg_link_color' ) . "; }
		.edit-post-visual-editor .has-secondary-background-color { color: " . themeModColors( 'secondarybg_text_color' ) . "; }
		.edit-post-visual-editor .has-secondary-background-color a { color: " . themeModColors( 'secondarybg_link_color' ) . "; }
		.edit-post-visual-editor .has-tertiary-background-color { color: " . themeModColors( 'tertiarybg_text_color' ) . "; }
		.edit-post-visual-editor .has-tertiary-background-color a { color: " . themeModColors( 'tertiarybg_link_color' ) . "; }
		";

		wp_add_inline_style( 'autosite-admin-styles', $custom_css );

	}
	add_action( 'admin_enqueue_scripts', 'autosite_admin_scripts' );
}
