<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

register_nav_menus( array(
	'main_menu' => 'Main Menu',
) );

$nav_position = get_theme_mod( 'autosite_navigation_position' );

if ( 'split' == $nav_position ) {
	register_nav_menus( array(
		'secondary' => 'Secondary Menu',
	) );
}
