<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$mods = get_theme_mods();

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<title><?php wp_title(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

	<?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
	<?php wp_head(); ?>

	<?php if( get_theme_mod('autosite_google_analytics') ) { 
			$ga_id = get_theme_mod( 'autosite_google_analytics' );
	?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-<?php echo esc_attr( get_theme_mod('autosite_google_analytics')); ?>"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-<?php echo esc_attr( get_theme_mod('autosite_google_analytics') ); ?>');
	</script>
	<?php } ?>

</head>
<body <?php body_class(); ?>>

	<header role="banner" id="masthead" class="<?php echo get_theme_mod('autosite_header_bgcolor') . '-bg'; ?><?php if ( 'fixed' == get_theme_mod('autosite_fixed_header') ) { echo ' fixed'; } ?><?php echo ' header-' . get_theme_mod('autosite_navigation_position'); ?>">

		<?php get_template_part( 'template-parts/customizations/headers/header', get_theme_mod('autosite_navigation_position') ); ?>
		
	</header>

	<main role="main">

		<div class="<?php echo esc_attr( get_theme_mod('autosite_container_type') ); ?> wrapper<?php echo ' ' . esc_attr( get_theme_mod('autosite_container_width') ) . '-container'; ?> flex-container">