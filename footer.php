<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container 			= get_theme_mod( 'autosite_container_type' );
$container_width 	= get_theme_mod('autosite_container_width' );
$footer_bg 			= get_theme_mod( 'autosite_footer_bgcolor' );

?>

	</div>

</main>

<footer role="contentinfo" class="<?php if ( $footer_bg ) { echo esc_attr( $footer_bg ) . '-bg'; } else { echo 'primary-bg'; } ?>">

	<div class="<?php echo esc_attr( $container ) . ' '; ?>wrapper<?php echo ' ' . esc_attr( $container_width ) . '-container'; ?>">

		<p class="copyright"><?php autosite_copyright(); ?></p>

	</div>

</footer>

<?php wp_footer(); ?>

</body>
</html>