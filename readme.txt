=== Theme Name ===
Contributors: morganmspencer
Requires at least: 5.0
Tested up to: 5.2
Requires PHP: 5.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Short description. No more than 150 chars.

== Description ==
The Autosite theme is a total solution for your WordPress website. It combines the powerful functionality of the Block Editor with the flexibility of the Customizer. No page builders or advanced customizations needed!

== Frequently Asked Questions ==

= What makes your theme different? =

Our theme is one of the most customizable themes out there with nearly limitless combinations for a completely unique website. It makes full use of the customizer to provide you with live preview updates as you modify your website. This also allows you to schedule updates for your site design. Anyone at any experience level can make full use of our theme!

== Changelog ==

= 1.0.0 =
* Theme launch!

== Upgrade Notice ==

= 1.0 =
* Upgrade notices describe the reason a user should upgrade.  No more than 300 characters.