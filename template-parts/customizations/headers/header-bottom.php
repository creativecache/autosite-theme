<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

?>

<div class="wrapper flex-container flex-between flex-middle<?php echo ' ' . esc_attr( get_theme_mod( 'autosite_container_type' ) ); ?><?php echo ' ' . esc_attr( get_theme_mod( 'autosite_header_padding' ) ) . '-padding'; ?><?php echo ' ' . esc_attr( get_theme_mod( 'autosite_container_width' ) ) . '-container'; ?>">

	<div class="site-identity flex-item flex-middle">

		<a class="site-brand<?php if ( has_custom_logo() ) { echo ' custom-logo'; } ?><?php echo ' ' . esc_attr( get_theme_mod(' autosite_header_size' ) ) . '-text'; ?>" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="Go to <?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?> homepage" itemprop="url">

		<?php if ( is_front_page() && is_home() ) : ?>
			<h1>
		<?php endif; ?>

				<?php 

					if ( ! has_custom_logo() ) {
						bloginfo( 'name' );
					} else {
						echo wp_get_attachment_image( get_theme_mod( 'custom_logo' ), 'full' );
					} 

				?>

		<?php if ( is_front_page() && is_home() ) : ?>
			</h1>
		<?php endif; ?>

		</a>

	</div>

	<div class="mobile-menu-toggle-container flex-item">
		<button class="menu-toggle<?php echo ' ' . esc_attr( get_theme_mod( 'autosite_menu_button' ) ); ?><?php echo ' ' . esc_attr( get_theme_mod( 'autosite_main_nav_color' ) ) . '-color'; ?>"><span class="screen-reader-text">Toggle main menu</span></button>
	</div>

</div>

<?php if ( has_nav_menu( 'main_menu' ) ): ?>
	<nav role="navigation" aria-label="main navigation" id="main-nav" class="<?php echo esc_attr( get_theme_mod( 'autosite_main_nav_bgcolor' ) ) . '-bg'; ?>">

		<div class="wrapper<?php echo ' ' . esc_attr( get_theme_mod( 'autosite_container_type' ) ); ?>">

			<?php wp_nav_menu( array(
				'theme_location' => 'main_menu',
				'menu_id'        => 'main-menu',
				'menu_class'     => 'flex-container flex-between',
				'container' 	 => '',
			) ); ?>

		</div>

	</nav>
<?php endif; ?>