<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'autosite_container_type' );
$nav_position = get_theme_mod( 'autosite_navigation_position' );

?>

<?php if ( has_nav_menu( 'main_menu' ) ): ?>
	<nav role="navigation" aria-label="main navigation" id="main-nav" class="<?php if ( 'right' == $nav_position ) : ?>flex-item flex-right<?php endif; ?>">

		<?php if ( 'bottom' == $nav_position ) : ?>
			<div class="<?php echo esc_attr( $container ); ?> wrapper">
		<?php endif; ?>

			<?php wp_nav_menu( array(
				'theme_location' => 'main_menu',
				'menu_id'        => 'main-menu',
				'menu_class'     => 'flex-container flex-between',
				'container' 	 => '',
			) ); ?>

		<?php if ( 'bottom' == $nav_position ) : ?>
			</div>
		<?php endif; ?>

	</nav>
<?php endif; ?>