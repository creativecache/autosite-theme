<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$mods = get_theme_mods();

?>

<style type="text/css">

<?php if( ( get_theme_mod('autosite_main_nav_bgcolor') ) && ( ( get_theme_mod('autosite_navigation_position') == 'bottom' ) || ( get_theme_mod('autosite_navigation_position') == 'overlay' ) ) ) : ?>
	#main-nav {
	<?php // Main navigation background color
	if( ( get_theme_mod('autosite_main_nav_bgcolor') ) ) : ?>
		<?php if( ( get_theme_mod('autosite_main_nav_bgcolor') == 'primary' ) ) { ?>
		background-color: <?php echo get_theme_mod('autosite_primary_color'); ?>;
		<?php } elseif ( ( get_theme_mod('autosite_main_nav_bgcolor') == 'tertiary' ) ) { ?>
		background-color: <?php echo get_theme_mod('autosite_tertiary_color'); ?>;
		<?php } elseif ( ( get_theme_mod('autosite_main_nav_bgcolor') == 'white' ) ) { ?>
		background-color: #ffffff;
		<?php } elseif ( ( get_theme_mod('autosite_main_nav_bgcolor') == 'black' ) ) { ?>
		background-color: #000000;
		<?php } else { ?>
		background-color: <?php echo get_theme_mod('autosite_secondary_color'); ?>;
		<?php } ?>
	<?php endif; ?>
	}
<?php endif; ?>

<?php if( ( get_theme_mod('autosite_main_nav_color') ) || ( get_theme_mod('autosite_main_nav_text_transform') ) || ( get_theme_mod('autosite_main_nav_font_weight') ) ) : ?>
	#main-menu li a {
	<?php // Main nav text color
	if( ( get_theme_mod('autosite_main_nav_color') ) ) : ?>
	<?php if( ( get_theme_mod('autosite_main_nav_color') == 'primary' ) ) { ?>
	color: <?php echo get_theme_mod('autosite_primary_color'); ?>;
	<?php } elseif ( ( get_theme_mod('autosite_main_nav_color') == 'secondary' ) ) { ?>
	color: <?php echo get_theme_mod('autosite_secondary_color'); ?>;
	<?php } elseif ( ( get_theme_mod('autosite_main_nav_color') == 'tertiary' ) ) { ?>
	color: <?php echo get_theme_mod('autosite_tertiary_color'); ?>;
	<?php } elseif ( ( get_theme_mod('autosite_main_nav_color') == 'white' ) ) { ?>
	color: #ffffff;
	<?php } else { ?>
	color: #000000;
	<?php } ?>
	<?php endif; ?>

	<?php // Main nav text transform
	if( ( get_theme_mod('autosite_main_nav_text_transform') ) ) : ?>
	text-transform: <?php echo get_theme_mod('autosite_main_nav_text_transform'); ?>;
	<?php endif; ?>

	<?php // Main nav text transform
	if( ( get_theme_mod('autosite_main_nav_font_weight') ) ) : ?>
	font-weight: <?php echo get_theme_mod('autosite_main_nav_font_weight'); ?>;
	<?php endif; ?>
	}
<?php endif; ?>

<?php if( ( 'hide' != get_theme_mod('autosite_show_tagline') ) && ( get_theme_mod('autosite_header_color') ) ) : ?>
	.tagline { 
	<?php if( ( get_theme_mod('autosite_header_color') == 'primary' ) ) { ?>
		color: <?php echo get_theme_mod('autosite_primary_color'); ?>;
		<?php } elseif ( ( get_theme_mod('autosite_header_color') == 'secondary' ) ) { ?>
		color: <?php echo get_theme_mod('autosite_secondary_color'); ?>;
		<?php } elseif ( ( get_theme_mod('autosite_header_color') == 'tertiary' ) ) { ?>
		color: <?php echo get_theme_mod('autosite_tertiary_color'); ?>;
		<?php } elseif ( ( get_theme_mod('autosite_header_color') == 'black' ) ) { ?>
		color: #000000;
		<?php } else { ?>
		color: #ffffff;
		<?php } ?>
	}
<?php endif; ?>

<?php // Footer text color
if( get_theme_mod('autosite_footer_color') ) : ?>
	footer {
		<?php if( ( get_theme_mod('autosite_footer_color') == 'primary' ) ) { ?>
		color: <?php echo get_theme_mod('autosite_primary_color'); ?>;
		<?php } elseif ( ( get_theme_mod('autosite_footer_color') == 'tertiary' ) ) { ?>
		color: <?php echo get_theme_mod('autosite_tertiary_color'); ?>;
		<?php } elseif ( ( get_theme_mod('autosite_footer_color') == 'secondary' ) ) { ?>
		color: <?php echo get_theme_mod('autosite_secondary_color'); ?>;
		<?php } elseif ( ( get_theme_mod('autosite_footer_color') == 'black' ) ) { ?>
		color: #000000;
		<?php } else { ?>
		color: #ffffff;
		<?php } ?>
	}
<?php endif; ?>

<?php // Footer link color
if( get_theme_mod('autosite_footer_link_color') ) : ?>
	footer a {
		<?php if( ( get_theme_mod('autosite_footer_link_color') == 'primary' ) ) { ?>
		color: <?php echo get_theme_mod('autosite_primary_color'); ?>;
		<?php } elseif ( ( get_theme_mod('autosite_footer_link_color') == 'tertiary' ) ) { ?>
		color: <?php echo get_theme_mod('autosite_tertiary_color'); ?>;
		<?php } elseif ( ( get_theme_mod('autosite_footer_link_color') == 'white' ) ) { ?>
		color: #ffffff;
		<?php } elseif ( ( get_theme_mod('autosite_footer_link_color') == 'black' ) ) { ?>
		color: #000000;
		<?php } else { ?>
		color: <?php echo get_theme_mod('autosite_secondary_color'); ?>;
		<?php } ?>
	}
<?php endif; ?>

</style>