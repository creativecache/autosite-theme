<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header">
		
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<div class="entry-meta">

			<?php // Entry meta goes here ?>

		</div>

	</header>

	<div class="entry-content">

		<?php the_content(); ?>

	</div>

</article>
