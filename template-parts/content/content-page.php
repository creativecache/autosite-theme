<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="page-header">
		
		<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>

	</header>

	<div class="page-content">

		<?php the_content(); ?>

	</div>

</article>