<?php

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header(); 

$sidebar = get_theme_mod( 'autosite_sidebar_position' );
$post_sidebar = get_theme_mod( 'autosite_post_sidebar' );

?>

<?php if ( ($sidebar == 'left') && ($post_sidebar == 'show') ) { get_sidebar(); } ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'template-parts/content/content' ); ?>

	<?php
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;
	?>

<?php endwhile; ?>

<?php  if ( ($sidebar == 'right') && ($post_sidebar == 'show') ) { get_sidebar(); } ?>

<?php get_footer(); ?>