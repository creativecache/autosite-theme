<?php
/**
 * The template for displaying all single posts
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Autosite
 * @since 1.0.0
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header(); ?>

<section <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="page-header">
		
		<h1 class="page-title"><?php esc_html_e( 'Oops! That page cannot be found.', 'autosite' ); ?></h1>

	</header>

	<div class="page-content">

		<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'autosite' ); ?></p>

		<?php get_search_form(); ?>

		<?php the_widget( 'WP_Widget_Recent_Posts' ); ?>

	</div>

</section>

<?php get_footer(); ?>