<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Autosite
 * @since 1.0.0
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'template-parts/content/content', 'page' ); ?>

<?php endwhile; ?>

<?php get_footer(); ?>